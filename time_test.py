from random import randint
from timeit import timeit

import merge

functions = [
        merge.simple_merge,
        merge.iter_merge,
        merge.gen_merge,
        merge.heapq_merge,
        merge.counter_merge,
        merge.sort_merge,
        merge.pop_merge,
        merge.reverse_pop_merge,
        ]


def test_time(params1, params2, number):
    """
        params: {
            'minlen': ,
            'maxlen': ,
            'minval': ,
            'maxval': ,
            }
    """
    results = {func.__name__: 0 for func in functions}
    print(results)

    for _ in range(number):
        len1 = randint(params1['minlen'], params1['maxlen'])
        len2 = randint(params2['minlen'], params2['maxlen'])
        list1 = sorted([randint(params1['minval'], params1['maxval']) for _ in range(len1)])
        list2 = sorted([randint(params2['minval'], params2['maxval']) for _ in range(len2)])

        for func in functions:
            results[func.__name__] += timeit('merge(list1, list2)', number=100, globals={'merge': func, 'list1': list1[:], 'list2': list2[:]})

    return results

def time_test():
    params1 = {"minlen": 1, "maxlen": 50, 'minval': 1, 'maxval': 1000}
    params2 = {"minlen": 1, "maxlen": 50, 'minval': 1, 'maxval': 1000}
    results = test_time(params1, params2, 100)

    for func in functions:
        print(func.__name__, results[func.__name__])
    return results


if __name__ == '__main__':
    time_test()

